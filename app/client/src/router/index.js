import Vue from 'vue'
import Router from 'vue-router'
import Home from '../components/Home'
import Login from '../components/Login'
import Main from '../components/Main'

Vue.use(Router)

function guardMyroute()
{
  var isAuthenticated= false;
  //this is just an example. You will have to find a better or 
  // centralised way to handle you localstorage data handling 
  if(sessionStorage.authentication && sessionStorage.authentication !== 'null') {
    isAuthenticated = true;
  }
  return isAuthenticated;
}

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/users-management',
      name: 'user-management',
      redirect: (to) => {
        window.location.href = 'http://192.168.75.33/admin'
      }
    },
    {
      path: '/game',
      name: 'main',
      component: Main
    },
    {
      path: '/home',
      redirect: '/'
    }
  ],
})

router.beforeEach((to, from, next) => {
  let isAuthenticated = guardMyroute();
  if (to.name !== 'login' && to.name !== 'home' && to.name !== 'user-management' 
      && !isAuthenticated) next({ name: 'login' })
  else next()
})

export default router;