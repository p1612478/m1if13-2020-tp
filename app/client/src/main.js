// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import App from './App'
import router from './router'
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { dom } from '@fortawesome/fontawesome-svg-core'
// Timer
import VueCountdownTimer from 'vuejs-countdown-timer'
// Axios 
import axios from 'axios'
import VueAxios from 'vue-axios'
// Leaflet
import "leaflet/dist/leaflet.css"
import * as L from 'leaflet'

L.Icon.Default.imagePath = '/dist/';
// VueX
import VueX from 'vuex'

// Font awesome
dom.watch() 
library.add(fas) 
Vue.component('font-awesome-icon', FontAwesomeIcon)

// ElementUI, Axios
Vue.prototype.$http = axios
Vue.use(ElementUI)
Vue.use(axios, VueAxios)
Vue.use(VueX)
Vue.use(VueCountdownTimer)

Vue.config.productionTip = false

const store = new VueX.Store({
  state: {
    position: {
      lat: '45.782',
      lon: '4.8656',
      zoom: 15
    },
    image: '',
    user: {
      username: '',
      authentication: '',
      status: 'alive',
      trophies: []
    },
    listUsers: [],
    settings: {
      ttl: 1,
      perimeter: {
        lat1: '',
        lat2: '',
        lon1: '',
        lon2: '',
      },
      target: {
        lat: '',
        lon: ''
      }
    }
  },
  mutations: {
    initStore(state) {
			// Check if the ID exists
			if(sessionStorage.username) {
				// Replace the with the stored item
				state.user.username = sessionStorage.username
      }
      if(sessionStorage.authentication) {
				// Replace the with the stored item
				state.user.authentication = sessionStorage.authentication
      }
    },    
    updateMap(state, payload) {
      state.position.lat = payload.lat;
      state.position.lon = payload.lon;
      state.position.zoom = payload.zoom;
    },
    updatePosition(state, payload) {
      state.position.lat = payload.lat;
      state.position.lon = payload.lon;
      state.position.zoom = payload.zoom;
    },
    updateImage(state, payload) {
      state.image = payload.image;
    },
    updateUser(state, payload) {
      state.user.username = payload.username ? payload.username : state.user.username;
      state.user.authentication = payload.authentication ? payload.authentication : state.user.authentication;
      state.user.status = payload.status ? payload.status : state.user.status;
      state.user.trophies = payload.trophies ? payload.trophies : state.user.trophies;
      sessionStorage.username = payload.username ? payload.username : null;
      sessionStorage.authentication = payload.authentication ? payload.authentication : null;
    },
    updateListUsers(state, payload) {
      state.listUsers = payload.listUsers ? payload.listUsers : state.listUsers;
    },
    userLoggedOut(state) {
      state.user.username = '';
      state.user.authentication = '';
      sessionStorage.removeItem('username');
      sessionStorage.removeItem('authentication');
    },
    updatePerimeter(state, payload) {
      state.settings.perimeter.lat1 = payload.lat1 ? payload.lat1 : state.settings.perimeter.lat1;
      state.settings.perimeter.lat2 = payload.lat2 ? payload.lat2 : state.settings.perimeter.lat2;
      state.settings.perimeter.lon1 = payload.lon1 ? payload.lon1 : state.settings.perimeter.lon1;
      state.settings.perimeter.lon2 = payload.lon2 ? payload.lon2 : state.settings.perimeter.lon2;
    },
    focusPerimeter() {},
    updateTarget(state, payload) {
      state.settings.target.lat = payload.lat ? payload.lat : state.settings.target.lat;
      state.settings.target.lon = payload.lon ? payload.lon : state.settings.target.lon;
    },
    focusTarget() {},
    updateTTL(state, payload) {
      state.settings.ttl = payload.ttl ? payload.ttl : state.settings.ttl;
    },
    joinGameByPlayer() {},
    timeOut() {},
    userLost() {},
    userWon() {},
    gameFinished() {}
  },
  getters: {
    position: state => {
      return state.position
    },
    listUsers: state => {
      return state.listUsers
    },
    image: state => {
      return state.image
    },
    user: state => {
      return state.user
    },
    ttl: state => {
      return state.settings.ttl
    },
    perimeter: state => {
      return state.settings.perimeter
    },
    target: state => {
      return state.settings.target
    },
    isAuthenticated: state => !!state.authentication,
  }
})

new Vue({
  el: '#app',
  store: store,
  router,
  components: { App },
  template: '<App/>',
  beforeCreate() {
    this.$store.commit('initStore');
  }
})
