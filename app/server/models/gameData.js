//** Game data
  //* Game status
  let gameStatus = "inactive" //> AdminOnly: "inactive" when inactive, "active" when started and "finished"
  let gameWinner = undefined
  let gameTarget = {}
  let gamePerimeter = {
    topLeft: {},
    bottomRight: {}
  }
  //* Player data
  let playerData = {
    id : 'user_1',
    url : 'http://example.com/users/toto/avatar.png',
    role : "player", //> player or admin
    status : 'waiting', //> player status, 4 posibble values [ waiting, playing, won, eliminated ]
    position : [ 0, 1, 2 ], //> track player positions of each round in game
    ttl : 1, //> time limit for 1 round, default value is 1 min
    trophy : [] //> TODO: implement player trophy e.g match MVP, etc.
  }
  let otherPlayer = {
    id : 'user_2',
    url : 'http://example.com/users/tintin/avatar.png',
    role : "player", //> player or admin
    status : 'waiting', //> player status, 4 posibble values [ waiting, playing, won, eliminated ]
    position : [ 0, 1 ], //> track player positions of each round in game
    ttl : 1, //> time limit for 1 round, default value is 1 min
    trophy : [] //> TODO: implement player trophy e.g match MVP, etc.
  }
  //* Admin data
  let adminData = {
    id : 'admin',
    url : 'http://example.com/users/admin/avatar.png',
    role : 'admin',
    status : 'waiting' //> only 2 values accepted [ waiting, playing ]
  }
  //* User list
  let userList = [
    adminData,
    playerData,
    otherPlayer
  ]

  //* Change User's position
  let setUserPosition = (idLogin, positions) => {
    userList.forEach((userObj) => {
      if (userObj.id === idLogin && userObj.hasOwnProperty('position')) {
        userObj.position = positions
      }
    })
  }

  //* Change User's image
  let setUserImage = (idLogin, url) => {
    userList.forEach((userObj) => {
      if (userObj.id === idLogin && userObj.hasOwnProperty('url')) {
        userObj.url = url
      }
    })
  }

  //* Change user status
let setUserStatus = (idLogin, status) => {
  userList.forEach((userObj) => {
    if (userObj.id === idLogin)
      userObj.status = status
  })
}

//** Public data
module.exports = {
  status : gameStatus,
  target: gameTarget,
  perimeter: gamePerimeter,
  userList : userList,
  gameWinner: gameWinner,
  setUserPosition : setUserPosition,
  setUserImage : setUserImage,
  setUserStatus : setUserStatus
}