
// const SpringBootOrigin = 'http://localhost:8080'
// // const SpringBootOrigin = 'http://195.168.75.33:8080'
// const ExpressAPI = 'http://localhost:3376/api'
// // const ExpressAPI = 'http://195.168.75.33:3376/api'

const SpringBootOrigin = 'http://192.168.75.33/api-spring'
const ExpressAPI = 'http://192.168.75.33/api'

module.exports = {
	SpringBootOrigin: SpringBootOrigin,
	ExpressAPI: ExpressAPI
}