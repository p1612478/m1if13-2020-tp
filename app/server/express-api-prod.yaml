swagger: "2.0"
info:
  description: "This game is a simple race to a target."
  version: "1.0.0"
  title: "MifRace"
  license:
    name: "CC-BY-NC"
    url: "http://creativecommons.org/licenses/by-nc/4.0/"
  contact:
    name: "Lionel Médini"
    url: "https://perso.liris.cnrs.fr/lionel.medini"
host: "192.168.75.33"
basePath: "/api"
tags:
  - name: "georesources"
    description: "Server-side geolocalized resources"
schemes:
  - "http"
  - "https"
paths:
  /resources:
    get:
      tags:
        - "georesources"
      summary: "Get all living resources"
      description: "Returns an array containing the representations of all existing resources in the game if the user is alive, or only that of the user if she is dead"
      produces:
        - "application/json"
      responses:
        200:
          description: "successful operation"
          schema:
            type: "array"
            items:
              $ref: "#/definitions/GeoResource"
        401:
          description: "User authentication failed"
      security:
        - bearerAuth: []
  /resources/{resourceId}/position:
    put:
      tags:
        - "georesources"
      summary: "Update user's position"
      description: "Send a LatLng object to the server."
      consumes:
        - "application/json"
      parameters:
        - name: "resourceId"
          in: "path"
          description: "User's login"
          required: true
          type: "string"
        - name: position
          in: "body"
          description: "User's position"
          required: true
          schema:
            $ref: "#/definitions/LatLng"
      responses:
        200:
          description: "successful operation"
        400:
          description: "Invalid position object"
        401:
          description: "User authentication failed"
        404:
          description: "User not found"
      security:
        - bearerAuth: []
  /resources/{resourceId}/image:
    put:
      tags:
        - "georesources"
      summary: "(re)set user's image URL"
      description: "Sets or updates the user's photo/icon/... image file URL"
      consumes:
        - "text/plain"
      parameters:
        - name: "resourceId"
          in: "path"
          description: "User's login"
          required: true
          type: "string"
        - name: "url"
          in: "body"
          required: true
          schema:
            type: "string"
      responses:
        204:
          description: "successful operation"
        400:
          description: "Invalid image URL object"
        401:
          description: "User authentication failed"
        404:
          description: "User not found"
      security:
        - bearerAuth: []
  /game:
    post:
      tags:
        - game
      summary: Create a new game, existing game will be overridden
      description: Send a target and its perimeter to initialize a game
      consumes:
        - application/json
      parameters:
        - name: data
          in: body
          required: true
          schema:
            type: object
            properties:
              target:
                $ref: '#/definitions/GPSCoord'
              perimeter:
                type: object
                properties:
                  topLeft:
                    $ref: '#/definitions/GPSCoord'
                  bottomRight:
                    $ref: '#/definitions/GPSCoord'
      responses:
        204:
          description: "successful operation"
        400:
          description: "invalid data"
        401:
          description: "User authentication failed"
      security:
        - bearerAuth: []
    get:
      tags:
        - game
      summary: Check current game status
      description: Check current game status, always works
      produces:
        - application/json
      responses:
        200:
          description: ok
          schema:
            $ref: '#/definitions/GameObj'
      security:
        - bearerAuth: []
  /game/start:
    post:
      tags:
        - game
      summary: Start the game
      description: start the game
      responses:
        204:
          description: game started
        401:
          description: unauthorized
      security:
        - bearerAuth: []
securityDefinitions:
  bearerAuth:
    type: apiKey
    in: header
    name: Authentication
definitions:
  GeoResource:
    type: "object"
    required:
      - "id"
    properties:
      id:
        type: "string"
        description: "User login or element id"
        example: "toto"
      url:
        type: "string"
        description: "image/icon URL"
        example: "http://example.com/users/toto/avatar.png"
      position:
        $ref: "#/definitions/LatLng"
      role:
        type: "string"
        description: "Resource role in the game"
        enum:
          - "player"
          - "target"
      ttl:
        type: "integer"
        description: "For users only: remaining time in seconds before the end of the game"
      status:
        type: "string"
        enum:
          - "playing"
          - "eliminated"
          - "element"
      trophys:
        type: "array"
        description: "For users only: other users that the current user has eliminated, and number of targets that she has reached first"
        items:
          $ref: "#/definitions/Trophy"
  LatLng:
    type: "array"
    items:
      type: "number"
    minItems: 2
    maxItems: 2
    description: "LatLng object as defined in Leaflet"
  Trophy:
    type: "object"
    required:
      - "id"
    properties:
      id:
        $ref: "#/definitions/GeoResource/properties/id"
      action:
        type: "string"
        enum:
          - "eliminated"
          - "reached"
    description: "Description of a trophy over an existing resource in the game; TO BE MODIFIED..."
  GPSCoord:
    type: object
    properties:
      longitude:
        type: number
        multipleOf: 0.1
      latitude:
        type: number
        multipleOf: 0.1
    example:
      { longitude: 0.0, latitude: 0.0 }
  GameObj:
    type: object
    properties:
      status:
        type: boolean
      target:
        $ref: '#/definitions/GPSCoord'
      perimeter:
        type: object
        properties:
          topLeft:
            $ref: '#/definitions/GPSCoord'
          bottomRight:
            $ref: '#/definitions/GPSCoord'
      playerList:
        type: array
        items:
          $ref: '#/definitions/GeoResource'
externalDocs:
  description: "See M1IF13 practicals 2020 #3"
  url: "https://forge.univ-lyon1.fr/LIONEL.MEDINI/m1if13-2020/-/tree/master/tp3"