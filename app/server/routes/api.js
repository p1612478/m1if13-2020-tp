//** Init API
const express = require('express')
const axios = require('axios')
const bodyParser = require('body-parser')
const config = require('../config')
let router = express.Router()
router.use(express.urlencoded({ extended: true }))
router.use(express.json())
router.use(bodyParser.text())
  //** Game Variables
  let gameData = require('../models/gameData')
  //* Authorization
  async function authorize(req) {
    try {
      const userToken = req.get('Authentication');
      const origin = config.SpringBootOrigin
      const requestURL = config.SpringBootOrigin + '/authenticate'
      const response = await axios.get(requestURL, {
        params : {
          token : userToken,
          origin : origin
        }
      })
      return response.status
    } catch (error) {
      console.log(error);
      console.log('API: authentication error')
      return 500
    }
  }
  //* Verify User
  async function verifyUser(idLogin) {
    try {
      const requestURL = config.SpringBootOrigin + '/users/' + idLogin
      const response = await axios.get(requestURL)
      return {
        responseCode: response.status,
        responseData: response.data
      }
    } catch (error) {
      console.log(error);
      console.log('API: verifyUser error')
      return { responseCode: 500 }
    }
  }

//** Middleware that is specific to this router
router.use(function timeLog (req, res, next) {
  authorize(req).then((responseCode) => {
    switch (responseCode) {
      case 204:
        next()
        break
      default:
        res.status(401).send()
    }
  })
})

//** API requests
  //* debug route
  router.get('/debug', function (req, res) {
    res.send('Debug API Module : see console log')
  })

  /**
   * GET all Resources
   */
  router.get('/resources', (req, res) => {
      const responseObj = gameData.userList
      res.status(200).json(responseObj)
  })

  /**
   * PUT Update user's position
   * change player status if the goal is reached
   */
  router.put('/resources/:idResource/position', (req, res) => {
    const idResource = req.params.idResource //> user's ID
    verifyUser(idResource).then((responseObj) => {
      switch (responseObj.responseCode) {
        case 200:
          const data = req.body.position
          console.log(data);
          if (!Array.isArray(data)) {
            res.status(400).send()
            break
          }
          //> change user position
          gameData.setUserPosition(idResource, data)
          //> update status if possible
          const origin = [data[0],data[1]]
          const dest = [gameData.target.latitude, gameData.target.longitude]
          if (gameData.status === "active" && gameData.target.latitude !== undefined && gameData.target.longitude !== undefined
          && distance(origin, dest) < 2) {
            gameData.setUserStatus(idResource, 'won')
            gameData.status = "finished"
            res.status(200).send({ message: 'user reached the goal'})
          } else if (gameData.status === "active") {
            res.status(200).send({ message: 'user\'s position changed' })
          } else if (gameData.status === "finished") {
            res.status(200).send({ message: 'game finished' })
          }
          break
        default:
          res.status(404).send()
      }
    })
  })
  //* calculate distance between two GPS coords
  let toRadian = (degree) => {
    return degree*Math.PI/180;
  }
  let distance = (origin, destination) => {
    // return distance in meters
    let lon1 = toRadian(origin[1]),
      lat1 = toRadian(origin[0]),
      lon2 = toRadian(destination[1]),
      lat2 = toRadian(destination[0]);

    let deltaLat = lat2 - lat1;
    let deltaLon = lon2 - lon1;

    let a = Math.pow(Math.sin(deltaLat/2), 2) + Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin(deltaLon/2), 2);
    let c = 2 * Math.asin(Math.sqrt(a));
    let EARTH_RADIUS = 6371;
    return c * EARTH_RADIUS * 1000;
  }

  /**
   * PUT /resources/:idResource/image
   * Update user's image
   */
  router.put('/resources/:idResource/image', (req, res) => {
    const idResource = req.params.idResource //> user's ID
    verifyUser(idResource).then((responseObj) => {
      switch (responseObj.responseCode) {
        case 200:
          const data = req.body
          if (typeof data !== 'string') {
            res.status(400).send()
            break
          }
          //> change user image
          gameData.setUserImage(idResource, data)
          res.status(204).send()
          break
        default:
          res.status(404).send()
      }
    })
  })

/**
   * POST /game
   * Admin Only
   * Create a game, will override existing game in server
   * request.body = {
   *   perimeter = {
   *     topLeft : { longitude, latitude },
   *     bottomRight : { longitude, latitude }
   *   },
   *   target = { longitude, latitude }
   * }
   */
  router.post('/game', (req, res) => {
    const data = req.body
    verifyGameData(data).then((responseObj) => {
      switch(responseObj.responseCode) {
        case 200:
          gameData.perimeter = responseObj.perimeter
          gameData.target = responseObj.target
          gameData.status = "active"
          gameData.userList.forEach((user) => {
            user.status = 'waiting'
          })
          res.status(204).send()
          break
        default:
          res.status(400).send()
      }
    })
  })
  //* Verify game data
  let verifyGameData = async (data) => {
    let checkData = data.hasOwnProperty('target') && data.hasOwnProperty('perimeter')
    checkData = checkData && data.perimeter.hasOwnProperty('topLeft') && data.perimeter.hasOwnProperty('bottomRight')
    if (!checkData) return { responseCode: 400 }

    const target = data.target
    let checkTarget = target.hasOwnProperty('longitude') && target.hasOwnProperty('latitude')
    if (!checkTarget) return { responseCode: 400 }

    const perimeter = data.perimeter
    let checkPerimeter = perimeter.hasOwnProperty('topLeft') && perimeter.hasOwnProperty('bottomRight') &&
      perimeter.topLeft.hasOwnProperty('longitude') && perimeter.topLeft.hasOwnProperty('longitude') &&
      perimeter.bottomRight.hasOwnProperty('longitude') && perimeter.bottomRight.hasOwnProperty('longitude')
    if (!checkPerimeter) return { responseCode: 400 }

    return {
      responseCode: 200,
      perimeter: perimeter,
      target: target
    }
  }

  /**
   * GET /game
   * Admin Only
   * See game status
   */
  router.get('/game', (req, res) => {
    const output = {
      status: gameData.status,
      target: gameData.target,
      perimeter: gameData.perimeter,
      playerList: gameData.userList
    }
    res.status(200).json(output)
  })

/**
 * POST /game/start
 * Start game
 */
router.post('/game/start', (req, res) => {
  gameData.status = "active";
  gameData.userList.forEach((user) => {
    user.status = 'playing'
  })
  res.status(204).send()
})

//** Public properties
module.exports = router;