let express = require('express');
let router = express.Router();
const axios = require('axios')
const bodyParser = require('body-parser')
const config = require('../config')
const session = require('express-session');
router.use(express.urlencoded({ extended: true }))
router.use(express.json())
router.use(bodyParser.text())

// add & configure middleware
router.use(session({
    secret: 'secret key',
    resave: false,
    saveUninitialized: true
}))

var sess;

const url = config.SpringBootOrigin
const urlExpressAPI = config.ExpressAPI
const adminLoginURL = '/admin/login'
// middleware that is specific to this router
router.use(function timeLog (req, res, next) {
    // console.log('Time: ', Date.now());
    next()
});
// default admin page
router.get('/', function(req, res) {
    sess=req.session;
    if(sess.logged) {
        res.render('pages/admin-home', {userRoute: '/admin/users', 
                                        settingsGameRoute: '/admin/settings',
                                        gameViewRoute: '/admin/view'}
        );
    } else {
        res.redirect('/admin/login');
    }
   
});

/**
 * GET /admin/login
 */
router.get('/login', function(req, res) {
    res.render('pages/login');
})

//======== GAME SETTINGS ========//
/**
 * GET /admin/settings
 * access game setting input form
 */
router.get('/settings', function(req, res) {
    sess=req.session;
    if(sess.logged) {
        res.render('pages/game-settings');
    } else {
        res.redirect('/admin/login');
    }    
})
/**
 * POST /admin/settings
 * create new game
 */
router.post('/settings', (req, res) => {
    sess = req.session
    if (sess.logged) {
        const data = req.body
        console.log(urlExpressAPI + '/game')
        console.log(sess.authentication)
        axios.post(urlExpressAPI + '/game', data, {
            headers: { 'Authentication': sess.authentication }
        }).then((response) => {
              res.status(204).send({message: 'Game created'})
        }).catch((error) => {
              res.status(error.response.status).send({message: 'Something went wrong'})
        })
    } else res.redirect(adminLoginURL)
})

/**
 * GET /admin/view
 * view game status
 */
router.get('/view', function(req, res) {
    sess=req.session;
    if(sess.logged) {
        axios.get(urlExpressAPI + '/game', {
            headers: { 'Authentication': sess.authentication }
        }).then((response) => {
            const data = response.data
            let output = {}
            output.status = data.status
            output.target = data.target === {} ? null : data.target
            output.topLeft = data.perimeter.topLeft === {} ? null : data.perimeter.topLeft
            output.bottomRight = data.perimeter.bottomRight === {} ? null : data.perimeter.bottomRight
            output.playerList = data.playerList
            console.log(data)
            res.render('pages/game-view', output)
        }).catch((error) => {
            console.log(error.response.status)
        })
    } else {
        res.redirect('/admin/login');
    }    
})

/**
 * GET /admin/users
 * get user list
 */
router.get('/users', function(req, res) {
    // Get list of users 
    axios.get(url + '/users').then(function(response) {
        const users = response.data;
        res.render('pages/users-management', {status: 'success', users: users});
    }).catch(function (error) {
        res.render('pages/users-management', {status: 'error', users: null, error: error});
    });
});

/**
 * GET /admin/users/:loginID
 */
router.get('/users/:login', function(req, res) {
    // Get user
    var login = req.params.login;
    axios.get(url + '/users/' + login).then(function(response) {
        res.status(200).send(response.data);
    });
});

/**
 * POST /admin/users
 * create new user
 */
router.post('/users', function(req, res) {
    var data = {
        login: req.body.login,
        password: req.body.password,
        connected: req.body.connected
    }
    axios.post(url + '/users', data).then(function(response) {
        res.status(200).send({message:'User has been successfully updated'});
    }).catch(function (error) {
        if (error.response.status === 400) {
            res.status(error.response.status).send('This user already existed');
        } else {
            res.status(error.response.status).send('Failed to create this user');
        }
    });
});

/**
 * POST /users/login
 * login user
 */
router.post('/users/login', function(req, res) {
    sess = req.session;
    var postUrl = url + '/login'
    if (req.body.login !== 'admin') res.status(401).send('Unauthorized')
    else {
        axios({
            method: 'POST',
            url: postUrl,
            data: {
                login: req.body.login,
                password: req.body.password
            },
        }).then(function (response) {
            sess.logged = true;
            sess.authentication = response.headers.authentication;
            res.status(200)
              .send({message: 'User has been successfully logged in'});
        }).catch(function (error) {
            res.status(404).send('Failed to log in');
        });
    }
});

/**
 * DELETE /admin/users/logout
 * logout user
 */
router.delete('/users/logout', function(req, res) {
    sess = req.session;
    var deleteUrl = url + "/logout";
    axios.delete(deleteUrl, {
        headers: { 'Authentication': sess.authentication }
      }).then(function(response) {
        sess.logged = false;
        sess.authentication = null;
        res.status(200).send({message:'User has been successfully logged out'});
    }).catch(function (error) {
        res.status(404).send('Failed to log out');        
    });
});

/**
 * PUT /admin/users/:loginID
 * modify an user
 */
router.put('/users/:login', function(req, res) {
    var login = req.params.login;
    var data = {
        login: req.body.login,
        password: req.body.password,
        connected: false
    }
    axios.put(url + '/users/' + login, data).then(function(response) {
        res.status(200).send({message:'User has been successfully created'});
    }).catch(function (error) {
        if (error.response.status === 400) {
            res.status(error.response.status).send('This user login already existed');
        } else {
            res.status(error.response.status).send('Failed to create this user');
        }
    });
});

/**
 * DELETE /admin/users/:loginID
 * delete an user
 */
router.delete('/users/:login', function(req, res) {
    var login = req.params.login;
    axios.delete(url + '/users/' + login).then(function(response) {
        res.status(200).send({message:'User has been successfully deleted'});
    }).catch(function (error) {
        console.log(error.response);
        if (res.response.status === 404) {
            res.status(error.response.status).send('This user does not exist');
        } else {
            res.status(500).send('Failed to delete user');
        }
    });
});

module.exports = router;