let showSuccess = (message) => {
	$('#userModal').modal({show: true});
	$('.modal-title').html('Success');
	$('.modal-body p').html(message);
	if(!$('.modal .btn-primary').hasClass('hidden')) {
		$('.modal .btn-primary').addClass('hidden');
	}
	// Reload page after modal closed
	$('#userModal').on('hidden.bs.modal', function () {
		location.reload();
	});
}

let showError = (message) => {
	$('#userModal').modal({show: true});
	$('.modal-title').html('Error');
	$('.modal-body p').html(message);
	if(!$('.modal .btn-primary').hasClass('hidden')) {
		$('.modal .btn-primary').addClass('hidden');
	}
}

let confirmUserAction = () => {
	let topLeft = {
		longitude: parseFloat($('#lon1').val()),
		latitude: parseFloat($('#lat1').val())
	}
	let bottomRight = {
		longitude: parseFloat($('#lon2').val()),
		latitude: parseFloat($('#lat2').val())
	}
	let target = {
		longitude: parseFloat($('#targetLon').val()),
		latitude: parseFloat($('#targetLat').val())
	}
	if (topLeft.latitude === null && topLeft.longitude === null)
		showError('Empty longitude/latitude 1')
	if (bottomRight.latitude === null && bottomRight.longitude === null)
		showError('Empty longitude/latitude 2')
	if (target.latitude === null && target.longitude === null)
		showError('Empty longitude/latitude for target')
	// send request to express API
	createGame({
		target: target,
		perimeter: {
			topLeft,
			bottomRight
		}
	})
}

function restartUserAction() {
	if (!$('.user-action').hasClass('hidden')) {
		$('.user-action').addClass('hidden');
	}
	$('#lon1').val(null); $('#lat1').val(null);
	$('#lon2').val(null); $('#lat2').val(null);
	$('#targetLon').val(null); $('#targetLat').val(null);
}

let createGame = (data) => {
	$.ajax({
		url: '/admin/settings',
		type: 'POST',
		data: JSON.stringify(data),
		dataType: 'json',
		contentType: 'application/json'
	}).done(() => {
		restartUserAction()
		showSuccess('Game created')
	}).fail((error) => {
		showError(error.responseText)
	})
}