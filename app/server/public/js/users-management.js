var current_user;
var mode;

function displayAddUser() {
    mode = 'create';
    displayByMode();
    $('#user-action-title').html('New user');
    if ($('.user-action .btn-success').hasClass('hidden')) {
        $('.user-action .btn-success').removeClass('hidden');
    }
    $('#pseudo').val(null);
    $('#password').val(null);
    $('#confirm-password').val(null);
}

function displayEditUser(user_login) {
    mode = 'edit';
    displayByMode();
    showUser(user_login);
}


function displayViewUser(user_login) {
    mode = 'view';
    displayByMode();
    showUser(user_login);    
}

function displayDeleteUser(user_login) {
    mode = 'delete';
    current_user = user_login;
    $('#userModal').modal({show: true});
    $('.modal-title').html('Delete confirmation');
    $('.modal-body p').html('Do you really want to delete this user (' + user_login + ') ?');
}

function displayByMode() {
    if (mode !== 'delete') {
        if ($('.user-action').hasClass('hidden')) {
            $('.user-action').removeClass('hidden');
        }
        if(mode === 'view') {
            if (!$('.user-action .confirm-password').hasClass('hidden')) {
                $('.user-action .confirm-password').addClass('hidden');
            }
            if (!$('.user-action .btn-success').hasClass('hidden')) {
                $('.user-action .btn-success').addClass('hidden');
            }
            $('#pseudo').attr('readonly', true);
            $('#password').attr('readonly', true);
        } else {
            if ($('.user-action .btn-success').hasClass('hidden')) {
                $('.user-action .btn-success').removeClass('hidden');
            }
            if ($('.user-action .confirm-password').hasClass('hidden')) {
                $('.user-action .confirm-password').removeClass('hidden');
            }
            $('#pseudo').attr('readonly', false);
            $('#password').attr('readonly', false);
            $('#confirm-password').attr('readonly', false);
        }    
        $('.user-action').get(0).scrollIntoView();
    }    
}

function showUser(user_login) {
    var user_promise = getUser(user_login);
    user_promise.done(function (data) {
        var user = data;
        current_user = user;
        $('#user-action-title').html('User: ' + user.login);
        $('#pseudo').val(user.login);
        $('#password').val(user.password);
    })     
}

/*********************************************** REQUEST ****************************************************/
function getUser(user_login) {
    var get_url = "/admin/users/" + user_login;
    return $.ajax({
        url: get_url,
        type: 'GET'
    }).done(function(data, textStatus, jqXHR) {
        return data;
    }).fail(function(error) {
        showError(error);
    });
}

function createUser(login, password) {
    var postData = {
        login: login,
        password: password
    }
    var request_method = "POST";
    var url =  "/admin/users";

    $.ajax({
        url: url,
        type: request_method,
        data: postData
    }).done(function(data) {
        restartUserAction();
        showSuccess(data.message);
    }).fail(function(error) {
        showError(error.responseText);
    });
}

function updateUser(login, password) {
    var putData = {
        login: login,
        password: password,
        connected: current_user.connected
    }
    var request_method = "PUT";
    var url =  "/admin/users/" + current_user.login;

    $.ajax({
        url: url,
        type: request_method,
        data: putData
    }).done(function(data) {
        restartUserAction();
        showSuccess(data.message);
    }).fail(function(error) {
        showError(error.responseText);
    });
}

function deleteUser(login) {
    var request_method = "DELETE";
    var url =  "/admin/users/" + login;

    $.ajax({
        url: url,
        type: request_method,
    }).done(function(data) {
        showSuccess(data.message);
    }).fail(function(error) {
        showError(error.responseText);
    });
}

/***************************************************** MODAL ************************************************/
function showSuccess(message) {
    $('#userModal').modal({show: true});
    $('.modal-title').html('Success');
    $('.modal-body p').html(message);
    if(!$('.modal .btn-primary').hasClass('hidden')) {
        $('.modal .btn-primary').addClass('hidden');
    }
    // Reload page after modal closed
    $('#userModal').on('hidden.bs.modal', function () {
        location.reload();
    });
}

function showError(message) {
    $('#userModal').modal({show: true});
    $('.modal-title').html('Error');
    $('.modal-body p').html(message);
    if(!$('.modal .btn-primary').hasClass('hidden')) {
        $('.modal .btn-primary').addClass('hidden');
    }
}

function confirmModal() {
    if (mode === 'delete') {
        deleteUser(current_user);
    }
}

/*************************************************** USER ACTION FORM *****************************************/
function restartUserAction() {
    current_user = null;
    if (!$('.user-action').hasClass('hidden')) {
        $('.user-action').addClass('hidden');
    }
    $('#pseudo').val(null);
    $('#password').val(null);
    $('#confirm-password').val(null);
}

function confirmUserAction() {
    var login = $("#pseudo").val();
    var password = $("#password").val();
    var re_password = $("#confirm-password").val();
    if (login === '' || password === '') {
        showError('You need to fill both username and password');
    } else if (re_password !== password) {
        showError('Password and confirmation dont match');
    } else {
        if (mode === 'create') {
            createUser(login, password);
        } else if (mode === 'edit') {
            updateUser(login, password);
           }
    }
}

/******************************************** HIDE/SHOW PASSWORD ******************************************/
$("#password").password('toggle');
$("#confirm-password").password('toggle');
