function showSuccess(message) {
    $('#userModal').modal({show: true});
    $('.modal-title').html('Success');
    $('.modal-body p').html(message);
    if(!$('.modal .btn-primary').hasClass('hidden')) {
        $('.modal .btn-primary').addClass('hidden');
    }
    // Reload page after modal closed
    $('#userModal').on('hidden.bs.modal', function () {
        location.reload();
    });
}

function showError(message) {
    $('#userModal').modal({show: true});
    $('.modal-title').html('Error');
    $('.modal-body p').html(message);
    if(!$('.modal .btn-primary').hasClass('hidden')) {
        $('.modal .btn-primary').addClass('hidden');
    }
}
/*************************************************** USER ACTION FORM *****************************************/
function restartUserAction() {
    current_user = null;
    if (!$('.user-action').hasClass('hidden')) {
        $('.user-action').addClass('hidden');
    }
    $('#pseudo').val(null);
    $('#password').val(null);
}

function confirmUserAction() {
    var login = $("#pseudo").val();
    var password = $("#password").val();
    if (login === '' || password === '') {
        showError('You need to fill both username and password');
    }
    userLogIn(login, password);
}

function userLogIn(login, password) {
    const postData = {
        login: login,
        password: password
    }
    const request_method = "POST";
    const url =  "/admin/users/login";

    $.ajax({
        url: url,
        type: request_method,
        data: postData
    }).done(function(data) {
        restartUserAction();
        showSuccess(data.message);
        window.location.href="/admin";
    }).fail(function(error) {
        showError(error.responseText);
    });
}

/******************************************** HIDE/SHOW PASSWORD ******************************************/
$("#password").password('toggle');
$("#confirm-password").password('toggle');
