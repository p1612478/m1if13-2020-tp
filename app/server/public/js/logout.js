function showSuccess(message) {
    $('#userModal').modal({show: true});
    $('.modal-title').html('Success');
    $('.modal-body p').html(message);
    if(!$('.modal .btn-primary').hasClass('hidden')) {
        $('.modal .btn-primary').addClass('hidden');
    }
    // Reload page after modal closed
    $('#userModal').on('hidden.bs.modal', function () {
        location.reload();
    });
}

function showError(message) {
    $('#userModal').modal({show: true});
    $('.modal-title').html('Error');
    $('.modal-body p').html(message);
    if(!$('.modal .btn-primary').hasClass('hidden')) {
        $('.modal .btn-primary').addClass('hidden');
    }
}

/*************************************************** USER ACTION FORM *****************************************/
function logout(login, password) {
    var postData = {
        login: login,
        password: password
    }
    var request_method = "delete";
    var url =  "/admin/users/logout";

    $.ajax({
        url: url,
        type: request_method,
        data: postData
    }).done(function(data) {
        window.location.href="/admin";
    }).fail(function(error) {
    });
}
