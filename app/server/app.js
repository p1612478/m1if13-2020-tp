const express = require('express')
const cors = require('cors')
const app = express();
const port = 3376;


// set the view engine to ejs
app.set('view engine', 'ejs');
app.use(express.urlencoded({extended: true}))
app.use(cors())

// serve static file from /public e.g localhost:port/css/**.css
app.use('/static', express.static('public'));
// 

app.listen(port, () => console.log(`Example app listening on port ${port}!`));

// Default routes
app.get('/', (req, res) => res.redirect('/admin'));

/* ADMIN */
let admin = require('./routes/admin');
app.use('/admin', admin);

/* PUBLIC */
let pub = require('./routes/public');
app.use('/public', pub);

/* API */
let api = require('./routes/api');
app.use('/api', api);

/* error 500 */
app.use(function (err, req, res, next) {
    console.error(err.stack);
    res.status(500).send('Something broke!');
});

/* error 404 must be the last code*/
app.use(function (req, res, next) {
    res.status(404).send("Sorry can't find that!");
});