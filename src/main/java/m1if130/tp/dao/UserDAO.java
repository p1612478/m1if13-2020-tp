package m1if130.tp.dao;

import m1if130.tp.model.User;

import java.util.List;
import java.util.ArrayList;
import java.util.Optional;
import java.util.Set;
import java.util.HashSet;

public class UserDAO implements DAO<User> {
    private List<User> userList = new ArrayList<>();

    public UserDAO() {
        userList.add(new User("admin","admin"));
        userList.add(new User("user_1","user_1"));
        userList.add(new User("user_2","user_2"));
    }

    /**
     * Tester s'il existe 1 utilisateur connecté dans la liste
     *
     * @param login Login de l'utilisateur
     * @return Un boolean, true or false
     */
    public boolean isConnected(String login) {
        Optional<User> userOp = get(login);
        return userOp.isPresent() && userOp.get().isConnected();
    }

    /**
     *
     */
    public void disconnectUser(String login) {
        Optional<User> userOp = get(login);
        userOp.ifPresent(User::disconnect);
    }

    /**
     * Récupère un utilisateur
     *
     * @param id Login de l'utilisateur
     * @return Un java.util.Optional qui contient (ou pas) l'utilisateur
     */
    @Override
    public Optional<User> get(String id) {
        for (User u : userList) {
            if (u.getLogin().equals(id)) {
                return Optional.of(u);
            }
        }
        return Optional.empty();
    }

    /**
     * Récupère tous les utilisateurs
     *
     * @return Un Set de login
     */
    @Override
    public Set<String> getAll() {
        if (userList.isEmpty()) {
            return null;
        } else {
            Set<String> userSet = new HashSet<>();
            for (User u : userList) {
                userSet.add(u.getLogin());
            }
            return userSet;
        }
    }

    /**
     * Crée un utilisateur et le sauvegarde
     *
     * @param user L'utilisateur à créer
     */
    @Override
    public void save(User user) {
        if (!userList.contains(user)) {
            userList.add(user);
        }
    }

    /**
     * Modifie un utilisateur
     *
     * @param user   L'utilisateur à modifier
     * @param params Un tableau de **2** Strings : login et password
     */
    @Override
    public void update(User user, String[] params) {
        if (params.length != 2) return;
        Optional<User> userOp = get(user.getLogin());
        if (userOp.isPresent()) {
            int index = userList.indexOf(userOp.get());
            userList.set(index, new User(params[0], params[1]));
        }
    }

    /**
     * Supprime un utilisateur
     *
     * @param user L'utilisateur à supprimer
     */
    @Override
    public void delete(User user) {
        Optional<User> userOp = get(user.getLogin());
        userOp.ifPresent(value -> userList.remove(value));
    }
}
