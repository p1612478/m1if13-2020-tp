package m1if130.tp;

import m1if130.tp.config.UserConfig;
import m1if130.tp.dao.UserDAO;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class TpApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
		ctx.register(UserConfig.class); /* init UserList bean */
		/* get UserList */
		/* UserList userList = ctx.getBean(UserList.class); */
		ctx.refresh();

		SpringApplication.run(TpApplication.class, args);
	}

	@Bean
	public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
		return args -> {
			System.out.println("==DEBUG==");
			UserDAO userDAO = ctx.getBean(UserDAO.class);
			System.out.println("=="+userDAO.get("admin").get().getLogin());
		};
	}
}
