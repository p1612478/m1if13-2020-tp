package m1if130.tp.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class DefaultController {

    @GetMapping(
        value = "/",
        produces = MediaType.TEXT_HTML_VALUE
    )
    public ModelAndView index() {
        return new ModelAndView("index");
    }

}
