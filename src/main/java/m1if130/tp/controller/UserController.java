package m1if130.tp.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import m1if130.tp.dao.UserDAO;
import m1if130.tp.model.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping("/users")
public class UserController {

    /* récupérer le DAO */
    @Autowired
    @Qualifier("userDAO")
    private UserDAO userDAO;

    /**
     * Récupère tous les utilisateurs
     *
     * @return liste des utilisateurs or null
     */
    @GetMapping(
        value = "",
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Operation(
        summary = "Récupérer tous les utilisateurs.",
        description = "Récupérer tous les utilisateurs.",
        responses = {
            @ApiResponse(
                responseCode = "200",
                description = "ok",
                content = @Content(mediaType = "application/json",
                    examples = @ExampleObject(
                        value = " [\n \"user_1\",\"admin\" \n]"
                    )
                )
            ),
            @ApiResponse(
                responseCode = "404",
                description = "There is no user yet",
                content = @Content()
            )
        }
    )
    public ResponseEntity<Object> showAllUsers() {
        if (!userDAO.getAll().isEmpty()) {
            Set<String> listUsers = userDAO.getAll();
            return ResponseEntity.status(200).body(listUsers);
        } else {
            return ResponseEntity.status(404).build();
        }
    }

    /**
     * Consulter un utilisateur par son login.
     *
     * @param login Le login de l'utilisateur
     * @return User or null
     */
    @GetMapping(
        value = "/{login}",
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Operation(
        summary = "Consulter un utilisateur par son login.",
        description = "Consulter un utilisateur par son login.",
        responses = {
            @ApiResponse(
                responseCode = "200",
                description = "ok",
                content = @Content(mediaType = "application/json", schema = @Schema(implementation = User.class))
            ),
            @ApiResponse(
                responseCode = "404",
                description = "utilisateur non trouvé",
                content = @Content()
            )
        }
    )
    public ResponseEntity<User> showUser(
        @PathVariable("login") String login
    ) {
        Optional<User> userOp = userDAO.get(login);
        if (userOp.isPresent()) {
            User currentUser = userOp.get();
            return ResponseEntity.status(200).body(currentUser);
        } else {
            return ResponseEntity.status(404).build();
        }
    }

    /**
     * Créer un nouveau utilisateur.
     *
     * @param newUser le RequestBody contenant les infos du nouveau utilisateur
     * @return un ResponseEntity ayant un des code (204, 400)
     */
    @PostMapping(
        consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE },
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Operation(
        summary = "Créer un nouveau utilisateur.",
        description = "Créer un nouveau utilisateur.",
        responses = {
            @ApiResponse(
                responseCode = "204",
                description = "ok",
                content = @Content()
            ),
            @ApiResponse(
                responseCode = "400",
                description = "utilisateur existe déjà",
                content = @Content()
            )
        }
    )
    public ResponseEntity<Void> createUser(
        @RequestBody User newUser
    ) {
        /* si le login est déjà dans la liste, ERR 400 */
        if (userDAO.get(newUser.getLogin()).isPresent()) {
            return ResponseEntity.status(400).build();
        }
        /* sinon create new */
        userDAO.save(newUser);
        return ResponseEntity.status(204).build();
    }

    /**
     * Mettre à jour l'utilisateur.
     *
     * @param login le login de l'utilisateur
     * @param newUser le RequestBody contenant les infos du nouveau utilisateur
     * @return un ResponseEntity ayant un des code (204, 400)
     */
    @PutMapping(
        value = "/{login}",
        consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE },
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Operation(
        summary = "Mettre à jour l'utilisateur.",
        description = "Mettre à jour l'utilisateur.",
        responses = {
            @ApiResponse(
                responseCode = "204",
                description = "ok",
                content = @Content()
            ),
            @ApiResponse(
                responseCode = "400",
                description = "mise à jour impossible, le nouveau login existe déjà",
                content = @Content()
            )
        }
    )
    public ResponseEntity<Void> updateUser(
        @PathVariable String login,
        @RequestBody User newUser
    ) {
        Optional<User> userOp = userDAO.get(login);
        /* si le login est dans la liste, update */
        if (userOp.isPresent()) {
            User currentUser = userOp.get();
            if (!userDAO.get(newUser.getLogin()).isPresent() || newUser.getLogin().equals(currentUser.getLogin())) {
                String[] arr = new String[2];
                arr[0] = newUser.getLogin(); arr[1] = newUser.getPassword();
                userDAO.update(currentUser, arr);
                return ResponseEntity.status(204).build();
            } else {
                /* ERR 400, le nouveau login de newUser existe déjà, impossible de mettre à jour l'user */
                return ResponseEntity.status(400).build();
            }
        }
        /* sinon create new */
        else {
            if (!newUser.getLogin().equals(login)) {
                newUser.setLogin(login);
            }
            userDAO.save(newUser);
            return ResponseEntity.status(204).build();
        }
    }

    /**
     * Supprimer l'utilisateur.
     *
     * @param login le login de l'utilisateur
     * @return un ResponseEntity ayant un des codes (204, 404)
     */
    @DeleteMapping(
        value = "/{login}"
    )
    @Operation(
        summary = "Supprimer l'utilisateur.",
        description = "Supprimer l'utilisateur.",
        responses = {
            @ApiResponse(
                responseCode = "204",
                description = "ok",
                content = @Content()
            ),
            @ApiResponse(
                responseCode = "404",
                description = "utilisateur non trouvé",
                content = @Content()
            )
        }
    )
    public ResponseEntity<Void> deleteUser(
        @PathVariable String login
    ) {
        Optional<User> userOp = userDAO.get(login);
        /* si le login n'existe pas, ERR 404 */
        if (!userOp.isPresent()) {
            return ResponseEntity.status(404).build();
        }
        /* sinon, delete */
        else {
            userDAO.delete(userOp.get());
            return ResponseEntity.status(204).build();
        }
    }

}
