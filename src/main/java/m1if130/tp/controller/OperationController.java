package m1if130.tp.controller;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import m1if130.tp.dao.UserDAO;
import m1if130.tp.model.LoginForm;
import m1if130.tp.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.naming.AuthenticationException;
import java.util.Date;
import java.util.Optional;

@RestController
public class OperationController {

    /* récupérer le DAO */
    @Autowired
    @Qualifier("userDAO")
    private UserDAO userDAO;

    /**
     * Procédure de login "simple" d'un utilisateur
     * @param loginForm contient l'id et le mot de passe de l'utilisateur
     * @return Une ResponseEntity avec le JWT dans le header "Authentication" si le login s'est bien passé, et le code de statut approprié (204, 401 ou 404).
     */
    @PostMapping(
        value = "/login",
        consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE },
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Operation(
        summary = "Procédure de login \"simple\" d'un utilisateur",
        description = "Procédure de login \"simple\" d'un utilisateur",
        responses = {
            @ApiResponse(
                responseCode = "204",
                description = "ok",
                content = @Content()
            ),
            @ApiResponse(
                responseCode = "401",
                description = "mauvais password",
                content = @Content()
            ),
            @ApiResponse(
                responseCode = "404",
                description = "utilisateur non trouvé",
                content = @Content()
            )
        }
    )
    public ResponseEntity<Void> login(
        @RequestBody LoginForm loginForm,
        @RequestHeader(value = "Origin", required = false) String origin
    ) {
        String login = loginForm.getLogin();
        String password = loginForm.getPassword();
        /* vérifier login infos */
        /* si login n'existe pas dans la liste, ERR 404 */
        Optional<User> userOp = userDAO.get(login);
        if (!userOp.isPresent()) {
            return ResponseEntity.status(404).build();
        }
        /* authenticate user */
        User currentUser = userOp.get();
        try {
            currentUser.authenticate(password);
        } /* si login existe mais le password est faux, ERR 401 */
        catch (AuthenticationException e) {
            return ResponseEntity.status(401).build();
        }
        /* construire le JWT token */
        String subject = login;
        String issuer = "mif13";
        long currentTime = System.currentTimeMillis();
        Date issuedAt = new Date(currentTime);
        //Date expiredAt = new Date(currentTime + 1800000L); // 1800s ~ 30min expiration
        try {
            Algorithm algorithm = Algorithm.HMAC256("secret");
            String token = JWT.create()
                .withSubject(subject)
                .withIssuer(issuer)
                .withIssuedAt(issuedAt)
                //.withExpiresAt(expiredAt)
                .sign(algorithm);
            /* renvoyer la réponse 204 avec token dans header */
            return ResponseEntity
                .status(204)
                .header("Access-Control-Expose-Headers","authentication")
                .header("authentication", token)
                .build();
        } catch (JWTCreationException e) {
            return ResponseEntity.status(500).build();
        }
    }

    /**
     * Réalise la déconnexion
     *
     * @param token Le token JWT dans "Authentication"
     * @return une ResponseEntity ayant un des codes (204, 500, 401)
     */
    @DeleteMapping(
        value = "/logout"
    )
    @Operation(
        summary = "Réalise la déconnexion",
        description = "Réalise la déconnexion",
        responses = {
            @ApiResponse(
                responseCode = "204",
                description = "ok",
                content = @Content()
            ),
            @ApiResponse(
                responseCode = "401",
                description = "mauvais token",
                content = @Content()
            ),
            @ApiResponse(
                responseCode = "500",
                description = "erreur au décodage du token"
            )
        }
    )
    public ResponseEntity<Void> logout(
        @RequestHeader("Authentication") String token
    ) {
        /* init JWT vérificateur du token */
        try {
            DecodedJWT jwt = JWT.decode(token);
            String login = jwt.getSubject();
            if (userDAO.isConnected(login)) {
                userDAO.disconnectUser(login);
                return ResponseEntity.status(204).build();
            } else {
                return ResponseEntity.status(401).build();
            }
        } catch (JWTVerificationException e) {
            return ResponseEntity.status(500).build();
        }
    }

    /**
     * Méthode destinée au serveur Node pour valider l'authentification d'un utilisateur.
     * @param token Le token JWT qui se trouve dans le header "Authentication" de la requête
     *
     * @param origin L'origine de la requête (pour la comparer avec celle du client, stockée dans le token JWT)
     * @return Une réponse vide avec un code de statut approprié (204, 400, 401).
     */
    @GetMapping(
        value = "/authenticate",
        consumes = { MediaType.ALL_VALUE },
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Operation(
        summary = "Méthode destinée au serveur Node pour valider l'authentification d'un utilisateur.",
        description = "Méthode destinée au serveur Node pour valider l'authentification d'un utilisateur.",
        responses = {
            @ApiResponse(
                responseCode = "204",
                description = "ok",
                content = @Content()
            ),
            @ApiResponse(
                responseCode = "401",
                description = "mauvais token",
                content = @Content()
            ),
            @ApiResponse(
                responseCode = "500",
                description = "erreur au décodage du token"
            )
        }
    )
    public ResponseEntity<Void> authenticate(
        @RequestParam("token") String token,
        @RequestParam("origin") String origin
    ) {
        /* init JWT vérificateur du token */
        try {
            Algorithm algorithm = Algorithm.HMAC256("secret");
            JWTVerifier verifier = JWT.require(algorithm)
                .withIssuer("mif13")
                .build();
            verifier.verify(token); /* tester si le token a la bonne origine */
            /* tester le login */
            DecodedJWT jwt = JWT.decode(token);
            String login = jwt.getSubject();
            if (userDAO.isConnected(login)) {
                return ResponseEntity.status(204).build();
            } else {
                return ResponseEntity.status(401).build();
            }
        } catch (JWTVerificationException e) {
            return ResponseEntity.status(500).build();
        }
    }
}