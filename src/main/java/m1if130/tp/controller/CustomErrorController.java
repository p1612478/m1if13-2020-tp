package m1if130.tp.controller;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

@Controller
public class CustomErrorController implements ErrorController {

    @RequestMapping("/error")
    public ModelAndView handleError(HttpServletRequest request) {
        Object errCodeObject = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);

        if (errCodeObject != null) {
            int errCode = Integer.parseInt(errCodeObject.toString());
            ModelAndView mav = new ModelAndView("error");
            mav.addObject("errCode", errCode);
            return mav;
        }
        return null;
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}
