package m1if130.tp.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {

    public static final String DEFAULT_MEDIA_TYPE  = "application/json";

    /* configurer le contenu par défaut */
    @Override
    public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
        configurer.defaultContentType(MediaType.valueOf(DEFAULT_MEDIA_TYPE));
    }

    /* CORS support */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/login")
            .allowedOrigins("http://localhost","http://192.168.75.33","https://192.168.75.33","http://editor.swagger.io","https://editor.swagger.io")
            .allowedMethods("POST");

        registry.addMapping("/authenticate")
            .allowedOrigins("http://localhost","http://192.168.75.33","https://192.168.75.33","http://editor.swagger.io","https://editor.swagger.io")
            .allowedMethods("GET");

        registry.addMapping("/logout")
            .allowedOrigins("http://localhost","http://192.168.75.33","https://192.168.75.33","http://editor.swagger.io","https://editor.swagger.io")
            .allowedMethods("DELETE");

        registry.addMapping("/users/**")
            .allowedOrigins("http://localhost","http://192.168.75.33","https://192.168.75.33","http://editor.swagger.io","https://editor.swagger.io")
            .allowedMethods("GET","POST","DELETE","PUT");
    }
}
