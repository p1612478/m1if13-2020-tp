package m1if130.tp.config;

import m1if130.tp.dao.UserDAO;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UserConfig {
    @Bean
    public UserDAO userDAO() {
        return new UserDAO();
    }
}
