# M1IF13-2020-Web avancé
Projet du Web avancé M1if13.

### Equipe
p1612478 - TRAN Ngoc-minh     
p1510082 - NGUYEN Xuan Bao Nguyen

### Documentation API
* `/users-api.yaml` la documentation de l'API - elle se trouve aussi sur l'url:      
  http://192.168.75.33:8080/api-spring/swagger-ui.html
* `/app/server/express-api.yaml` la documentation de l'API Express

### Déploiements
* https://192.168.75.33 l'interface front en VueJS de l'application
* https://192.168.75.33/admin l'interface d'administration généré par serveur Express
* https://192.168.75.33/api-spring l'API du Spring
* https://192.168.75.33/api l'api Express

### Fonctionnement
* Pour commencer, vous vous connecter sur l'interface d'administration https://192.168.75.33/admin **(username: admin; mot de passe: admin)**
* Cette interface vous permet de: 
    * Gérer les users (création, modification et suppression). Par défault, il y a 2 autres users qui sont déjà créé
    * Paramétrer et lancer une partie en donnant les valeurs d'un périmètre et d'un cible
    * Suivre le déroulement de la partie créée
* Une fois la partie lancée, vous pouvez vous connecter sur l'interface du joueur https://192.168.75.33
* Cette interface vous permet de;
    * Mettre à jour l'image de l'user courant
    * Participer dans une partie disponible
* Sur l'interface du joueur, il y a:
    * Un plan sur lequel s'affiche le cible et le périmètre ainsi que la position actuelle
    * Un formulaire pour placer la position
    * Un timer pour vous donner le temps restant
* A la fin de la partie, une fenêtre s'affiche avec le résultat. Ensuite, vous attendez jusqu'à ce qu'il y a une autre partie lancée par l'administrateur
